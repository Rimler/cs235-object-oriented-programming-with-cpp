#include <iostream>
#include <iomanip>
using namespace std;

#include "IterativeFunctions.h"
#include "RecursiveFunctions.h"

void EnterToContinue()
{
    cout << endl << "Press ENTER to continue..." << endl;
    string buffer;
    getline( cin, buffer );
    cout << string( 25, '\n' );
}

int main()
{
    cout << left;
    cout << string( 80, '-' ) << endl;
    cout << "RECURSION TESTER" << endl;
    cout << string( 80, '-' ) << endl << endl;

    EnterToContinue();

    { // TEST SCOPE BEGIN
        cout << setw( 40 ) << "* PART 1: COUNT UP " << string( 40, '*' ) << endl;
        cout << endl << string( 5, '*' )
            << " TEST 1: start = 1, end = 5 "
            << string( 10, '*' ) << endl;
        cout << "EXPECTED OUTPUT:" << endl;
        cout << "CountUp_Iter(1, 5): 1 2 3 4 5 " << endl;
        cout << "CountUp_Rec(1, 5): 1 2 3 4 5 " << endl;
        cout << endl;
        cout << "ACTUAL OUTPUT:" << endl;
        cout << "CountUp_Iter(1, 5): ";
        CountUp_Iter(1, 5);
        cout << endl;
        cout << "CountUp_Rec(1, 5): ";
        CountUp_Rec(1, 5);
        cout << endl;
    } // TEST SCOPE END

    EnterToContinue();

    { // TEST SCOPE BEGIN
        cout << setw( 40 ) << "* PART 1: COUNT UP " << string( 40, '*' ) << endl;
        cout << endl << string( 5, '*' )
            << " TEST 2: start = 5, end = 10 "
            << string( 10, '*' ) << endl;
        cout << "EXPECTED OUTPUT:" << endl;
        cout << "CountUp_Iter(5, 10): 5 6 7 8 9 10 " << endl;
        cout << "CountUp_Rec(5, 10): 5 6 7 8 9 10 " << endl;
        cout << endl;
        cout << "ACTUAL OUTPUT:" << endl;
        cout << "CountUp_Iter(5, 10): ";
        CountUp_Iter(5, 10);
        cout << endl;
        cout << "CountUp_Rec(5, 10): ";
        CountUp_Rec(5, 10);
        cout << endl;
    } // TEST SCOPE END

    EnterToContinue();

    { // TEST SCOPE BEGIN
        cout << setw( 40 ) << "* PART 2: SUM UP " << string( 40, '*' ) << endl;
        cout << endl << string( 5, '*' )
            << " TEST 1: start = 1, end = 5 "
            << string( 10, '*' ) << endl;
        cout << "EXPECTED OUTPUT:" << endl;
        cout << "SumUp_Iter(1, 5): 15 " << endl;
        cout << "SumUp_Rec(1, 5): 15 " << endl;
        cout << endl;
        cout << "ACTUAL OUTPUT:" << endl;
        cout << "SumUp_Iter(1, 5): ";
        cout << SumUp_Iter(1, 5);
        cout << endl;
        cout << "SumUp_Rec(1, 5): ";
        cout << SumUp_Rec(1, 5);
        cout << endl;
    } // TEST SCOPE END

    EnterToContinue();

    { // TEST SCOPE BEGIN
        cout << setw( 40 ) << "* PART 2: SUM UP " << string( 40, '*' ) << endl;
        cout << endl << string( 5, '*' )
            << " TEST 2: start = 5, end = 10 "
            << string( 10, '*' ) << endl;
        cout << "EXPECTED OUTPUT:" << endl;
        cout << "SumUp_Iter(5, 10): 45 " << endl;
        cout << "SumUp_Rec(5, 10): 45 " << endl;
        cout << endl;
        cout << "ACTUAL OUTPUT:" << endl;
        cout << "SumUp_Iter(5, 10): ";
        cout << SumUp_Iter(5, 10);
        cout << endl;
        cout << "SumUp_Rec(5, 10): ";
        cout << SumUp_Rec(5, 10);
        cout << endl;
    } // TEST SCOPE END

    EnterToContinue();

    { // TEST SCOPE BEGIN
        cout << setw( 40 ) << "* PART 3: DISPLAY " << string( 40, '*' ) << endl;
        vector<string> list1 = {"CS134", "CS200", "CS235"};
        cout << endl << string( 5, '*' )
            << " TEST 1: list1 is { \"CS134\", \"CS200\", \"CS235\" } "
            << string( 10, '*' ) << endl;
        cout << "EXPECTED OUTPUT:" << endl;
        cout << "Display_Iter(list1): " << endl;
        cout << "0. CS 134, 1. CS200, 2. CS235," << endl << endl;
        cout << "Display_Rec(list1): " << endl;
        cout << "0. CS 134, 1. CS200, 2. CS235," << endl;
        cout << endl;
        cout << "ACTUAL OUTPUT:" << endl;
        cout << "Display_Iter(list1, 0): " << endl;
        Display_Iter( list1 );
        cout << endl << endl;
        cout << "Display_Rec(list1, 0): " << endl;
        Display_Rec( list1, 0 );
        cout << endl;
    } // TEST SCOPE END

    EnterToContinue();

    { // TEST SCOPE BEGIN
        cout << setw( 40 ) << "* PART 3: DISPLAY " << string( 40, '*' ) << endl;
        vector<string> list2 = {"Kabe", "Luna", "Pixel", "Korra"};
        cout << endl << string( 5, '*' )
            << " TEST 2: list2 is { \"Kabe\", \"Luna\", \"Pixel\", \"Korra\" } "
            << string( 10, '*' ) << endl;
        cout << "EXPECTED OUTPUT:" << endl;
        cout << "Display_Iter(list2):  " << endl;
        cout << "0. Kabe, 1. Luna, 2. Pixel, 3. Korra," << endl << endl;
        cout << "Display_Rec(list2, 0):  " << endl;
        cout << "0. Kabe, 1. Luna, 2. Pixel, 3. Korra," << endl;
        cout << endl;
        cout << "ACTUAL OUTPUT:" << endl;
        cout << "Display_Iter(list2): " << endl;
        Display_Iter( list2 );
        cout << endl << endl;
        cout << "Display_Rec(list2, 0): " << endl;
        Display_Rec( list2, 0 );
        cout << endl;
    } // TEST SCOPE END

    EnterToContinue();

    { // TEST SCOPE BEGIN
        cout << setw( 40 ) << "* PART 4: DISPLAY CHARS " << string( 40, '*' ) << endl;
        cout << endl << string( 5, '*' )
            << " TEST 1: string is \"Hello!\" "
            << string( 10, '*' ) << endl;
        cout << "EXPECTED OUTPUT:" << endl;
        cout << "DisplayChars_Iter( \"Hello!\" ): " << endl;
        cout << "0. H, 1. e, 2. l, 3. l, 4. o, 5. !," << endl << endl;
        cout << "DisplayChars_Rec( \"Hello!\", 0 ): " << endl;
        cout << "0. H, 1. e, 2. l, 3. l, 4. o, 5. !," << endl;
        cout << endl;
        cout << "ACTUAL OUTPUT:" << endl;
        cout << "DisplayChars_Iter( \"Hello!\" ): " << endl;
        DisplayChars_Iter( "Hello!" );
        cout << endl << endl;
        cout << "DisplayChars_Rec( \"Hello!\", 0 ): " << endl;
        DisplayChars_Rec( "Hello!", 0 );
        cout << endl;
    } // TEST SCOPE END

    EnterToContinue();

    { // TEST SCOPE BEGIN
        cout << setw( 40 ) << "* PART 4: DISPLAY CHARS " << string( 40, '*' ) << endl;
        cout << endl << string( 5, '*' )
            << " TEST 2: string is \"a b c\" "
            << string( 10, '*' ) << endl;
        cout << "EXPECTED OUTPUT:" << endl;
        cout << "DisplayChars_Iter( \"a b c\" ): " << endl;
        cout << "0. a, 1.  , 2. b, 3.  , 4. c," << endl << endl;
        cout << "DisplayChars_Rec( \"a b c\", 0 ): " << endl;
        cout << "0. a, 1.  , 2. b, 3.  , 4. c," << endl;
        cout << endl;
        cout << "ACTUAL OUTPUT:" << endl;
        cout << "DisplayChars_Iter( \"a b c\" ): " << endl;
        DisplayChars_Iter( "a b c" );
        cout << endl << endl;
        cout << "DisplayChars_Rec( \"a b c\", 0 ): " << endl;
        DisplayChars_Rec( "a b c", 0 );
        cout << endl;
    } // TEST SCOPE END

    EnterToContinue();

    { // TEST SCOPE BEGIN
        cout << setw( 40 ) << "* PART 5: FACTORIAL! " << string( 40, '*' ) << endl;
        cout << endl << string( 5, '*' )
            << " TEST 1: Calculate 5! "
            << string( 10, '*' ) << endl;
        cout << "EXPECTED OUTPUT:" << endl;
        cout << "Factorial_Iter( 5 ): 120" << endl << endl;
        cout << "Factorial_Rec( 5 ): 120" << endl;
        cout << endl;
        cout << "ACTUAL OUTPUT:" << endl;
        cout << "Factorial_Iter( 5 ): ";
        cout << Factorial_Iter( 5 );
        cout << endl << endl;
        cout << "Factorial_Rec( 5 ): ";
        cout << Factorial_Rec( 5 );
        cout << endl;
    } // TEST SCOPE END

    EnterToContinue();

    { // TEST SCOPE BEGIN
        cout << setw( 40 ) << "* PART 5: FACTORIAL! " << string( 40, '*' ) << endl;
        cout << endl << string( 5, '*' )
            << " TEST 2: Calculate 10! "
            << string( 10, '*' ) << endl;
        cout << "EXPECTED OUTPUT:" << endl;
        cout << "Factorial_Iter( 10 ): 3628800" << endl << endl;
        cout << "Factorial_Rec( 10 ): 3628800" << endl;
        cout << endl;
        cout << "ACTUAL OUTPUT:" << endl;
        cout << "Factorial_Iter( 10 ): ";
        cout << Factorial_Iter( 10 );
        cout << endl << endl;
        cout << "Factorial_Rec( 10 ): ";
        cout << Factorial_Rec( 10 );
        cout << endl;
    } // TEST SCOPE END

    EnterToContinue();

    cout << "The end!" << endl;

    return 0;
}
