#ifndef _ITERATIVE_FUNCTIONS_H
#define _ITERATIVE_FUNCTIONS_H
#include <vector>
#include <string>
using namespace std;

void CountUp_Iter(int start, int end);
int SumUp_Iter(int start, int end);
void Display_Iter(vector<string> list);
void DisplayChars_Iter(string str);
int Factorial_Iter( int n );

#endif
