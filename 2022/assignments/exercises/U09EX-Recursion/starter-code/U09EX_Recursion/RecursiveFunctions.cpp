#include "RecursiveFunctions.h"
#include <iostream> /* Use cout */
using namespace std;

// SET 1: Steps are given ------------------------------------------------------------

void CountUp_Rec(int start, int end) {
  // 1. TERMINATING CASE: If the `start` value is greater than the `end` value, then return.
  // 2. ACTION: Display the value of `start` and a space
  // 3. RECURSION: Call `CountUp_Rec` again, passing in `start+1` to move the start forward, and `end` staying the same.
}

int SumUp_Rec(int start, int end) {
  // 1. TERMINATING CASE: If the value of `start` is greater than `end`, then return `0`.
  // 2. RECURSIVE CASE: Return the value of `start` PLUS a call to `Sumup_Rec` with the arguments `start+1` and `end`.

  return 1; // TEMPORARY - REMOVE ONCE YOU IMPLEMENT THIS FUNCTION.
}

void Display_Rec(vector<string> list, unsigned int i) {
  // 1. TERMINATING CASE: If `i` is greater than or equal to `list.size()` then return.
  // 2. ACTION: Display the index and value of the item at that index.
  // 3. RECURSIVE CALL: Call `Display_Rec`, passing in the `list` and `i+1`.
}

// SET 2: Steps aren't given ------------------------------------------------------------

void DisplayChars_Rec(string str, unsigned int i)
{
    // Iterate over the string using recursion (you can use str.size() to get its size)
    // and display the index of each character and each character.
}

int Factorial_Rec( int n )
{
    // Use recursion to calculate the product of numbers from
    // n to 1. For example, n! = n (n-1) (n-2) ... (2) (1).
    // Note that    0! = 1

    return 1; // TEMPORARY - REMOVE ONCE YOU IMPLEMENT THIS FUNCTION.
}
