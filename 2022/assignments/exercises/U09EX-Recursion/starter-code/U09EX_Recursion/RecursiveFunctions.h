#ifndef _RECURSIVE_FUNCTIONS_H
#define _RECURSIVE_FUNCTIONS_H
#include <vector>
#include <string>
using namespace std;

void CountUp_Rec(int start, int end);
int SumUp_Rec(int start, int end);
void Display_Rec(vector<string> list, unsigned int i);
void DisplayChars_Rec(string str, unsigned int i);
int Factorial_Rec( int n );

#endif

