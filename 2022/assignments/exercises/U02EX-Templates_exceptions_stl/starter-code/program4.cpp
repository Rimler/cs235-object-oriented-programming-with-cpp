#include <iostream>
#include <vector>
using namespace std;

template <typename T>
void DisplayElement( vector<T> item, int index )
{
    // TODO: Add error check
    cout << item[index] << endl;
}

int GetSandwichesPerPerson( int totalSandwiches, int people )
{
    // TODO: Add error check
    return totalSandwiches / people;
}

void Program4()
{
    cout << endl << "TOWNS" << endl;
    int index;
    vector<string> towns = { "Overland Park", "Olathe", "Lenexa" };
    cout << "Enter index to display (0-2): ";
    cin >> index;
    
    // TODO: Add error check
    DisplayElement( towns, index );


    cout << endl << "SANDWICHES" << endl;
    int sandwiches;
    int people;

    cout << "How many sandwiches? ";
    cin >> sandwiches;

    cout << "How many people? ";
    cin >> people;

    // TODO: Add error check
    int sandwichesPerPerson = GetSandwichesPerPerson( sandwiches, people );
    cout << "You should have " << sandwichesPerPerson << " per 1 person" << endl;
}

