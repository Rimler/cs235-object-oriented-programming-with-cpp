

--------------------------------------------------
NOTES:


  
--------------------------------------------------
GRADING CRITERIA:

* 1. (10%) II-B. Use the vector template class. 
* 2. (10%) III-D. Handle exceptions caused by unusual or error conditions during program execution.
* 3. (10%) III-F. Create templates to develop data independent classes.
* 4. (30%) I. Software Development
*** a. Identify C++ features needed to solve problems in C++.
*** b. Define the problem and identify the classes.
*** c. Develop a solution.
*** d. Code the solution.
*** e. Test the solution.
* 5. (5%) Error checks and management
* 6. (5%) Clean, readable C++ code
* 7. (5%) Clean, readable user interface
* 8. (25%) Meeting all program requirements

--------------------------------------------------
RESULT:

** Criteria 1: 5/5 ==> 10%
** Criteria 2: 5/5 ==> 10%
** Criteria 3: 5/5 ==> 10%
** Criteria 4: 5/5 ==> 30%
** Criteria 5: 5/5 ==> 5%
** Criteria 6: 5/5 ==> 5%
** Criteria 7: 5/5 ==> 5%
** Criteria 8: 5/5 ==> 25%
