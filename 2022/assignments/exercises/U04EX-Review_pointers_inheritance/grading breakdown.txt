U04EX Pointers and Inheritance review

--------------------------------------------------
NOTES:


  
--------------------------------------------------
GRADING CRITERIA:

* 1. (20%) Create and use pointers
* 2. (20%) Dynamically allocate memory for variables and arrays
* 3. (10%) Use smart pointers
* 4. (10%) Use inheritance
* 5. (5%) Error checks and management
* 6. (5%) Clean, readable C++ code
* 7. (5%) Clean, readable user interface
* 8. (25%) Meeting all program requirements

--------------------------------------------------
RESULT:

** Criteria 1: 5/5 ==> 20%
** Criteria 2: 5/5 ==> 20%
** Criteria 3: 5/5 ==> 10%
** Criteria 4: 5/5 ==> 10%
** Criteria 5: 5/5 ==> 5%
** Criteria 6: 5/5 ==> 5%
** Criteria 7: 5/5 ==> 5%
** Criteria 8: 5/5 ==> 25%
