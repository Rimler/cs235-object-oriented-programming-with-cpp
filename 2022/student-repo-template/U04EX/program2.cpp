#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

struct Product
{
    string name;
    float price;
};

void Program2()
{
    cout << endl << "POINTERS AND OBJECTS" << endl;

    Product products[3];
    products[0].name = "Pencil";
    products[0].price = 1.59;

    products[1].name = "Markers";
    products[1].price = 2.39;

    products[2].name = "Eraser";
    products[2].price = 0.59;

    Product* ptrProduct = nullptr;

    cout << endl << "PRODUCTS:" << endl;
    for ( int i = 0; i < 3; i++ )
    {
        cout << left
            << setw( 5 ) << i
            << setw( 20 ) << products[i].name
            << setw( 10 ) << products[i].price << endl;
    }

    // TODO: Add code

    cout << endl << "THE END" << endl;
}
