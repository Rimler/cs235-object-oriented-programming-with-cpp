#include <iostream>
#include <string>
using namespace std;

void Program4()
{
    cout << endl << "DYNAMIC ARRAY" << endl;

    /*
    ABOUT DYNAMIC ARRAYS:
    Similarly, we can dynamically allocate space for an array on HEAP MEMORY by using pointers.
    This is how we create arrays that can be resized without knowing its size at compile-time.

    Remember that when we manually ALLOCATE space for a variable (using the "new []" keyword)
    we need to also manually DEALLOCATE space before the pointer loses scope (using the "delete []" keyword).
    */

    cout << endl << "DYNAMICALLY ALLOCATING MEMORY FOR ARRAYS" << endl << string( 80, '-' ) << endl;

    int arraySize = 3;
    int itemCount = 0;
    // Allocate space for a dynamic array named "arr", of size "arraySize", which stores strings.

    cin.ignore();

    bool done = false;
    while ( !done )
    {
        cout << endl << string( 40, '-' ) << endl;

        // Display array information
        cout << "ARRAY SIZE: " << arraySize << ", ITEM COUNT: " << itemCount << endl;
        cout << "ARRAY CONTENTS:" << endl;
        for ( int i = 0; i < itemCount; i++ )
        {
            // TODO
        }
        cout << endl << endl;

        cout << "Enter a new item to add, or QUIT to quit: ";
        string text;
        getline( cin, text );

        if ( text == "QUIT" )
        {
            done = true;
        }
        else
        {
            // Add a new item to the array, increment itemCount
            // TODO
        }

        // Check if full, resize if so
        if ( itemCount == arraySize )
        {
            cout << "* NEED TO RESIZE" << endl;
            int newSize = arraySize + 3;
            // TODO

            cout << "* COPY DATA FROM OLD ARRAY TO NEW ARRAY" << endl;
            for ( int i = 0; i < arraySize; i++ )
            {
            // TODO
            }

            cout << "* FREE THE OLD MEMORY" << endl;
            // TODO

            cout << "* UPDATE POINTER TO NEW MEMORY" << endl;
            // TODO

            cout << "* UPDATE ARRAYSIZE" << endl;
            // TODO
        }
    }

    cout << endl << "FREE REMAINING MEMORY BEFORE LEAVING!!" << endl;
    // Deallocate memory before the pointer goes out of scope


    cout << endl << "THE END" << endl;
}
