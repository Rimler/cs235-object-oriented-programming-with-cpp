# -- Color codes from --
# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux

# Reset
Color_Off='\033[0m'       # Text Reset

# Regular Colors
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White

# Background
On_Black='\033[40m'       # Black
On_Red='\033[41m'         # Red
On_Green='\033[42m'       # Green
On_Yellow='\033[43m'      # Yellow
On_Blue='\033[44m'        # Blue
On_Purple='\033[45m'      # Purple
On_Cyan='\033[46m'        # Cyan
On_White='\033[47m'       # White

# Variables for my test scripts
BuildingProgram="${Color_Off}-------------------------------------------------"
TestSpecs="\n ${Green}-------------------------------------------------"
ExpectedOutput="\n${Green}EXPECTED OUTPUT:"
ActualOutput="\n${Yellow}ACTUAL OUTPUT:"
FileOutput="\n${Purple}TEXT OUTPUT:"
SourceCode="\n${Purple} ${On_Black}SOURCE CODE:"
TestCases="\n${Cyan}TEST CASES:"
ProgramCode="${Blue}-------------------------------------------------"
ProgramDone="\n\n ${White}------- PRESS A KEY TO CONTINUE -------------------------------------------------"
Header="\n\n${White}-------------------------------------------------\n"


clear
 
echo -e $BuildingProgram
echo "Building..."
clang++ *.cpp -o exe

# ----------------------------------------------
echo -e $ProgramDone
read anykey

echo -e "${Header} PROGRAM 1"

echo -e $ExpectedOutput
echo "* EXPECTED OUTPUT: A list of variables with their values"
echo "* being displayed, as well as a list of 'max' versions for"
echo "* each pair."
echo -e $ActualOutput
echo 1 | ./exe

echo -e $ProgramDone
read anykey
echo -e $SourceCode
cat program1.cpp
echo -e $ProgramDone
read anykey

echo -e "${Header} PROGRAM 2"
echo -e $TestSpecs
echo "* Two vectors of items iterated through"
echo -e $ActualOutput
echo 2 | ./exe

echo -e $ProgramDone
read anykey
echo -e $SourceCode
cat program2.cpp
echo -e $ProgramDone
read anykey

echo -e "${Header} PROGRAM 3"
echo -e $TestSpecs
echo "* A map of basic state abbreviations / state names,"
echo "* A map of student IDs to Student objects."
echo "* Ability to type in a student ID to look up,"
echo "* should handle invalid keys properly."
echo -e $ActualOutput
echo 3 1 | ./exe
echo -e $ActualOutput
echo 3 100 | ./exe

echo -e $ProgramDone
read anykey
echo -e $SourceCode
cat program3.cpp
echo -e $ProgramDone
read anykey

echo -e "${Header} PROGRAM 4"
echo -e $TestSpecs
echo "* Asks for an index to view a town (0-2)."
echo "* Catch out_of_range exception"
echo ""
echo "* Asks for how many sandwiches and how many people."
echo "* Catch runtime_error exception"
echo -e $ActualOutput
echo "* valid inputs"
echo 4 0 12 6 | ./exe
echo -e $ActualOutput
echo "* invalid index"
echo 4 100 12 6 | ./exe
echo -e $ActualOutput
echo "*  division by 0"
echo 4 1 12 0 | ./exe

echo -e $ProgramDone
read anykey
echo -e $SourceCode
cat program4.cpp
echo -e $ProgramDone
read anykey


echo -e "${Header} PROGRAM 5"
echo -e $TestSpecs
echo "* Displays list of courses (from vector)"
echo "* Asks for index"
echo "* Catches out_of_range exceptions"
echo -e $ActualOutput
echo "* valid index"
echo 5 1 | ./exe
echo -e $ActualOutput
echo "* invalid index"
echo 5 100 | ./exe
echo -e $ProgramDone
read anykey
echo -e $SourceCode
cat program5.cpp
echo -e $ProgramDone
read anykey


