#include "FractionTester.h"
#include "Fraction.h"

#include <iostream>
using namespace std;

int FractionTester::s_score = 0;
int FractionTester::s_testCount = 0;

void FractionTester::RunTests()
{
    s_score = 0;
    s_testCount = 0;

    Test_DefaultConstructor();
    Test_ParameterizedConstructor();
    Test_CopyConstructor();
    Test_Set();
    Test_SetNumerator();
    Test_GetNumerator();
    Test_SetDenominator();
    Test_GetDenominator();
    Test_GetDecimal();
    Test_CommonDenominatorize();
    Test_Assignment();
    Test_Add();
    Test_Subtract();
    Test_Multiply();
    Test_Divide();
    Test_IsEqual();
    Test_IsNotEqual();
    Test_IsLessThan();
    Test_IsGreaterThan();
    Test_IsLessThanOrEqualTo();
    Test_IsGreaterThanOrEqualTo();

    cout << endl;
    cout << "TOTAL PASS:  " << s_score << endl;
    cout << "TOTAL TESTS: " << s_testCount << endl;
}

void FractionTester::Test_DefaultConstructor()
{
    cout << endl << string( 80, '-' ) << "* Test_DefaultConstructor" << endl;

    {
        cout << "TEST 1... ";
        s_testCount++;
        bool fail = false;

        Fraction frac;

        int expectedNum = 1, expectedDenom = 1;
        int actualNum = frac.m_num, actualDenom = frac.m_denom;

        if      ( actualNum != expectedNum )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected numerator!" << endl;
        }
        else if ( actualDenom != expectedDenom )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected denominator!" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  Fraction frac" << endl;
            cout << "\t frac: " << frac << endl;
            cout << "\t expected output: " << expectedNum << ", " << expectedDenom << endl;
            cout << "\t actual output:   " << actualNum << ", " << actualDenom << endl;
        }
    } // test end
}

void FractionTester::Test_ParameterizedConstructor()
{
    cout << endl << string( 80, '-' ) << "* Test_ParameterizedConstructor" << endl;

    {
        cout << "TEST 1... ";
        s_testCount++;
        bool fail = false;

        int inputNum = 2, inputDenom = 3;
        int expectedNum = 2, expectedDenom = 3;

        Fraction frac( inputNum, inputDenom );

        int actualNum = frac.m_num, actualDenom = frac.m_denom;

        if      ( actualNum != expectedNum )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected numerator!" << endl;
        }
        else if ( actualDenom != expectedDenom )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected denominator!" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  Fraction frac( 2, 3 )" << endl;
            cout << "\t frac: " << frac << endl;
            cout << "\t expected output: " << expectedNum << ", " << expectedDenom << endl;
            cout << "\t actual output:   " << actualNum << ", " << actualDenom << endl;
        }
    } // test end

    {
        cout << "TEST 2... ";
        s_testCount++;
        bool fail = false;

        int inputNum = 6, inputDenom = 4;
        int expectedNum = 6, expectedDenom = 4;

        Fraction frac( inputNum, inputDenom );

        int actualNum = frac.m_num, actualDenom = frac.m_denom;

        if      ( actualNum != expectedNum )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected numerator!" << endl;
        }
        else if ( actualDenom != expectedDenom )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected denominator!" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  Fraction frac( 6, 4 )" << endl;
            cout << "\t frac: " << frac << endl;
            cout << "\t expected output: " << expectedNum << ", " << expectedDenom << endl;
            cout << "\t actual output:   " << actualNum << ", " << actualDenom << endl;
        }
    } // test end
}

void FractionTester::Test_CopyConstructor()
{
    cout << endl << string( 80, '-' ) << "* Test_CopyConstructor" << endl;

    {
        cout << "TEST 1... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;
        frac1.m_num = 3;
        frac1.m_denom = 5;

        Fraction frac2( frac1 );

        int expectedNum = 3, expectedDenom = 5;
        int actualNum = frac2.m_num, actualDenom = frac2.m_denom;

        if      ( actualNum != expectedNum )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected numerator!" << endl;
        }
        else if ( actualDenom != expectedDenom )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected denominator!" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  Fraction frac2( frac1 )" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedNum << ", " << expectedDenom << endl;
            cout << "\t actual output:   " << actualNum << ", " << actualDenom << endl;
        }
    } // test end

    {
        cout << "TEST 2... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;
        frac1.m_num = 2;
        frac1.m_denom = 9;

        Fraction frac2( frac1 );

        int expectedNum = 2, expectedDenom = 9;
        int actualNum = frac2.m_num, actualDenom = frac2.m_denom;

        if      ( actualNum != expectedNum )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected numerator!" << endl;
        }
        else if ( actualDenom != expectedDenom )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected denominator!" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  Fraction frac2( frac1 )" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedNum << ", " << expectedDenom << endl;
            cout << "\t actual output:   " << actualNum << ", " << actualDenom << endl;
        }
    } // test end
}

void FractionTester::Test_Set()
{
    cout << endl << string( 80, '-' ) << "* Test_Set" << endl;

    {
        cout << "TEST 1... ";
        s_testCount++;
        bool fail = false;

        int inputNum = 1, inputDenom = 2;

        Fraction frac;
        frac.Set( inputNum, inputDenom );

        int expectedNum = 1, expectedDenom = 2;
        int actualNum = frac.m_num, actualDenom = frac.m_denom;

        if      ( actualNum != expectedNum )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected numerator!" << endl;
        }
        else if ( actualDenom != expectedDenom )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected denominator!" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t frac.Set( " << inputNum << ", " << inputDenom << " )" << endl;
            cout << "\t frac: " << frac << endl;
            cout << "\t expected num: " << expectedNum << endl;
            cout << "\t actual num: " << actualNum << endl;
            cout << "\t expected denom: " << expectedDenom << endl;
            cout << "\t actual denom: " << actualDenom << endl;
        }
    } // test end

    {
        cout << "TEST 2... ";
        s_testCount++;
        bool fail = false;

        int inputNum = 2, inputDenom = 3;

        Fraction frac;
        frac.Set( inputNum, inputDenom );

        int expectedNum = 2, expectedDenom = 3;
        int actualNum = frac.m_num, actualDenom = frac.m_denom;

        if      ( actualNum != expectedNum )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected numerator!" << endl;
        }
        else if ( actualDenom != expectedDenom )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected denominator!" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t frac.Set( " << inputNum << ", " << inputDenom << " )" << endl;
            cout << "\t frac: " << frac << endl;
            cout << "\t expected num: " << expectedNum << endl;
            cout << "\t actual num: " << actualNum << endl;
            cout << "\t expected denom: " << expectedDenom << endl;
            cout << "\t actual denom: " << actualDenom << endl;
        }
    } // test end
}

void FractionTester::Test_SetNumerator()
{
    cout << endl << string( 80, '-' ) << "* Test_SetNumerator" << endl;

    {
        cout << "TEST 1... ";
        s_testCount++;
        bool fail = false;

        Fraction frac;
        int input = 3;
        frac.SetNumerator( input );

        int expectedOutput = 3;
        int actualOutput = frac.m_num;

        if ( actualOutput != expectedOutput )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t SetNumerator( " << input << " )" << endl;
            cout << "\t frac: " << frac << endl;
            cout << "\t expected output: " << expectedOutput << endl;
            cout << "\t actual output: " << actualOutput << endl;
        }
    } // test end

    {
        cout << "TEST 2... ";
        s_testCount++;
        bool fail = false;

        Fraction frac;
        int input = 10;
        frac.SetNumerator( input );

        int expectedOutput = 10;
        int actualOutput = frac.m_num;

        if ( actualOutput != expectedOutput )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t SetNumerator( " << input << " )" << endl;
            cout << "\t frac: " << frac << endl;
            cout << "\t expected output: " << expectedOutput << endl;
            cout << "\t actual output: " << actualOutput << endl;
        }
    } // test end
}

void FractionTester::Test_GetNumerator()
{
    cout << endl << string( 80, '-' ) << "* Test_GetNumerator" << endl;

    {
        cout << "TEST 1... ";
        s_testCount++;
        bool fail = false;

        Fraction frac;
        frac.m_num = 10;

        int expectedNum = 10;
        int actualNum = frac.GetNumerator();

        if ( actualNum != expectedNum )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t frac m_num set to 10, using GetNumerator()" << endl;
            cout << "\t frac: " << frac << endl;
            cout << "\t expected num: " << expectedNum << endl;
            cout << "\t actual num: " << actualNum << endl;
        }
    } // test end

    {
        cout << "TEST 2... ";
        s_testCount++;
        bool fail = false;

        Fraction frac;
        frac.m_num = 7;

        int expectedNum = 7;
        int actualNum = frac.GetNumerator();

        if ( actualNum != expectedNum )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t frac m_num set to 7, using GetNumerator()" << endl;
            cout << "\t frac: " << frac << endl;
            cout << "\t expected num: " << expectedNum << endl;
            cout << "\t actual num: " << actualNum << endl;
        }
    } // test end
}

void FractionTester::Test_SetDenominator()
{
    cout << endl << string( 80, '-' ) << "* Test_SetDenominator" << endl;

    {
        cout << "TEST 1... ";
        s_testCount++;
        bool fail = false;

        Fraction frac;
        int input = 10;
        frac.SetDenominator( input );

        int expectedOutput = 10;
        int actualOutput = frac.m_denom;

        if ( actualOutput != expectedOutput )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t SetDenominator( " << input << " )" << endl;
            cout << "\t frac: " << frac << endl;
            cout << "\t expected output: " << expectedOutput << endl;
            cout << "\t actual output: " << actualOutput << endl;
        }
    } // test end

    {
        cout << "TEST 2... ";
        s_testCount++;
        bool fail = false;

        // Do the operation
        Fraction frac;
        frac.m_denom = 10;
        int input = 0;
        frac.SetDenominator( input );

        int expectedOutput = 10;
        int actualOutput = frac.m_denom;

        if ( actualOutput != expectedOutput )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t SetDenominator( " << input << " )" << endl;
            cout << "\t frac: " << frac << endl;
            cout << "\t expected output: " << expectedOutput << endl;
            cout << "\t actual output: " << actualOutput << endl;
        }
    } // test end
}

void FractionTester::Test_GetDenominator()
{
    cout << endl << string( 80, '-' ) << "* Test_GetDenominator" << endl;

    {
        cout << "TEST 1... ";
        s_testCount++;
        bool fail = false;

        Fraction frac;
        frac.m_denom = 10;

        int expectedDenom = 10;
        int actualDenom = frac.GetDenominator();

        if ( actualDenom != expectedDenom )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t frac m_denom set to 10, using GetDenominator()" << endl;
            cout << "\t frac: " << frac << endl;
            cout << "\t expected output: " << expectedDenom << endl;
            cout << "\t actual output:   " << actualDenom << endl;
        }
    } // test end

    {
        cout << "TEST 2... ";
        s_testCount++;
        bool fail = false;

        Fraction frac;
        frac.m_denom = 7;

        int expectedDenom = 7;
        int actualDenom = frac.GetDenominator();

        if ( actualDenom != expectedDenom )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t frac m_denom set to 7, using GetDenominator()" << endl;
            cout << "\t frac: " << frac << endl;
            cout << "\t expected output: " << expectedDenom << endl;
            cout << "\t actual output:   " << actualDenom << endl;
        }
    } // test end
}

void FractionTester::Test_GetDecimal()
{
    cout << endl << string( 80, '-' ) << "* Test_GetDecimal" << endl;

    {
        cout << "TEST 1... ";
        s_testCount++;
        bool fail = false;

        Fraction frac;
        frac.m_num = 5;
        frac.m_denom = 10;

        int expectedOutput = 0.5;
        int actualOutput = frac.GetDecimal();

        if ( actualOutput != expectedOutput )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  Set frac to 5/10" << endl;
            cout << "\t frac: " << frac << endl;
            cout << "\t expected output: " << expectedOutput << endl;
            cout << "\t actual output:   " << actualOutput << endl;
        }
    } // test end

    {
        cout << "TEST 2... ";
        s_testCount++;
        bool fail = false;

        Fraction frac;

        frac.m_num = 8;
        frac.m_denom = 2;

        int expectedOutput = 4;
        int actualOutput = frac.GetDecimal();

        if ( actualOutput != expectedOutput )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  Set frac to 8/2" << endl;
            cout << "\t frac: " << frac << endl;
            cout << "\t expected output: " << expectedOutput << endl;
            cout << "\t actual output:   " << actualOutput << endl;
        }
    } // test end
}

void FractionTester::Test_CommonDenominatorize()
{
    cout << endl << string( 80, '-' ) << "* Test_CommonDenominatorize" << endl;

    {
        cout << "TEST 1... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1( 1, 2 );
        Fraction frac2( 2, 3 );
        Fraction newFrac = frac1.CommonDenominatorize( frac2 );

        int expectedNum = 3, expectedDenom = 6;
        int actualNum = newFrac.m_num, actualDenom = newFrac.m_denom;

        if      ( actualNum != expectedNum )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected numerator!" << endl;
        }
        else if ( actualDenom != expectedDenom )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected denominator!" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1.CommonDenominatorize( frac2 )" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t newFrac: " << newFrac << endl;
            cout << "Expected output: " << expectedNum << "/" << expectedDenom << endl;
            cout << "Actual output:   " << actualNum << "/" << actualDenom << endl;
        }
    } // test end

    {
        cout << "TEST 2... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1( 2, 3 );
        Fraction frac2( 1, 4 );
        Fraction newFrac = frac1.CommonDenominatorize( frac2 );

        int expectedNum = 8, expectedDenom = 12;
        int actualNum = newFrac.m_num, actualDenom = newFrac.m_denom;

        if      ( actualNum != expectedNum )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected numerator!" << endl;
        }
        else if ( actualDenom != expectedDenom )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected denominator!" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1.CommonDenominatorize( frac2 )" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t newFrac: " << newFrac << endl;
            cout << "Expected output: " << expectedNum << "/" << expectedDenom << endl;
            cout << "Actual output:   " << actualNum << "/" << actualDenom << endl;
        }
    } // test end
}

void FractionTester::Test_Assignment()
{
    cout << endl << string( 80, '-' ) << "* Test_Assignment" << endl;

    {
        cout << "TEST 1... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;
        Fraction frac2;
        frac2.m_num = 3;
        frac2.m_denom = 4;

        frac1 = frac2;

        int expectedNum = 3, expectedDenom = 4;
        int actualNum = frac1.m_num, actualDenom = frac1.m_denom;

        if      ( actualNum != expectedNum )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected numerator!" << endl;
        }
        else if ( actualDenom != expectedDenom )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected denominator!" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 = frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedNum << ", " << expectedDenom << endl;
            cout << "\t actual output:   " << actualNum << ", " << actualDenom <<  endl;
        }

    } // test end

    {
        cout << "TEST 2... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;
        Fraction frac2;

        frac2.m_num = 7;
        frac2.m_denom = 8;

        frac1 = frac2;

        int expectedNum = 7, expectedDenom = 8;
        int actualNum = frac1.m_num, actualDenom = frac1.m_denom;

        if      ( actualNum != expectedNum )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected numerator!" << endl;
        }
        else if ( actualDenom != expectedDenom )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected denominator!" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 = frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedNum << ", " << expectedDenom << endl;
            cout << "\t actual output:   " << actualNum << ", " << actualDenom <<  endl;
        }
    } // test end
}

void FractionTester::Test_Add()
{
    cout << endl << string( 80, '-' ) << "* Test_Add" << endl;

    {
        cout << "TEST 1... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;
        frac1.m_num = 3;
        frac1.m_denom = 5;

        Fraction frac2;
        frac2.m_num = 7;
        frac2.m_denom = 11;

        Fraction result = frac1 + frac2;

        int expectedNum = 68, expectedDenom = 55;
        int actualNum = result.m_num, actualDenom = result.m_denom;

        if      ( actualNum != expectedNum )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected numerator!" << endl;
        }
        else if ( actualDenom != expectedDenom )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected denominator!" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 + frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedNum << ", " << expectedDenom << endl;
            cout << "\t actual output:   " << actualNum << ", " << actualDenom <<  endl;
        }
    } // test end

    {
        cout << "TEST 2... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;
        frac1.m_num = 1;
        frac1.m_denom = 2;

        Fraction frac2;
        frac2.m_num = 1;
        frac2.m_denom = 4;

        Fraction result = frac1 + frac2;

        int expectedNum = 6, expectedDenom = 8;
        int actualNum = result.m_num, actualDenom = result.m_denom;

        if      ( actualNum != expectedNum )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected numerator!" << endl;
        }
        else if ( actualDenom != expectedDenom )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected denominator!" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 + frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedNum << ", " << expectedDenom << endl;
            cout << "\t actual output:   " << actualNum << ", " << actualDenom <<  endl;
        }
    } // test end
}

void FractionTester::Test_Subtract()
{
    cout << endl << string( 80, '-' ) << "* Test_Subtract" << endl;

    {   cout << "TEST 1... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;
        frac1.m_num = 3;
        frac1.m_denom = 5;

        Fraction frac2;
        frac2.m_num = 7;
        frac2.m_denom = 11;

        Fraction result = frac1 - frac2;

        int expectedNum = -2, expectedDenom = 55;
        int actualNum = result.m_num, actualDenom = result.m_denom;

        if      ( actualNum != expectedNum )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected numerator!" << endl;
        }
        else if ( actualDenom != expectedDenom )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected denominator!" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 - frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedNum << ", " << expectedDenom << endl;
            cout << "\t actual output:   " << actualNum << ", " << actualDenom <<  endl;
        }
    } // test end

    {   cout << "TEST 2... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;
        frac1.m_num = 1;
        frac1.m_denom = 2;

        Fraction frac2;
        frac2.m_num = 1;
        frac2.m_denom = 4;

        Fraction result = frac1 - frac2;

        int expectedNum = 2, expectedDenom = 8;
        int actualNum = result.m_num, actualDenom = result.m_denom;

        if      ( actualNum != expectedNum )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected numerator!" << endl;
        }
        else if ( actualDenom != expectedDenom )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected denominator!" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 - frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedNum << ", " << expectedDenom << endl;
            cout << "\t actual output:   " << actualNum << ", " << actualDenom <<  endl;
        }
    } // test end
}

void FractionTester::Test_Multiply()
{
    cout << endl << string( 80, '-' ) << "* Test_Multiply" << endl;

    {
        cout << "TEST 1... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;
        frac1.m_num = 3;
        frac1.m_denom = 5;

        Fraction frac2;
        frac2.m_num = 7;
        frac2.m_denom = 11;

        Fraction result = frac1 * frac2;

        int expectedNum = 21, expectedDenom = 55;
        int actualNum = result.m_num, actualDenom = result.m_denom;

        if      ( actualNum != expectedNum )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected numerator!" << endl;
        }
        else if ( actualDenom != expectedDenom )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected denominator!" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 * frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedNum << ", " << expectedDenom << endl;
            cout << "\t actual output:   " << actualNum << ", " << actualDenom <<  endl;
        }
    } // test end

    {
        cout << "TEST 2... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;
        frac1.m_num = 1;
        frac1.m_denom = 2;

        Fraction frac2;
        frac2.m_num = 1;
        frac2.m_denom = 4;

        Fraction result = frac1 * frac2;

        int expectedNum = 1, expectedDenom = 8;
        int actualNum = result.m_num, actualDenom = result.m_denom;

        if      ( actualNum != expectedNum )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected numerator!" << endl;
        }
        else if ( actualDenom != expectedDenom )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected denominator!" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 * frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedNum << ", " << expectedDenom << endl;
            cout << "\t actual output:   " << actualNum << ", " << actualDenom <<  endl;
        }
    } // test end
}

void FractionTester::Test_Divide()
{
    cout << endl << string( 80, '-' ) << "* Test_Divide" << endl;

    {
        cout << "TEST 1... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;
        frac1.m_num = 3;
        frac1.m_denom = 5;

        Fraction frac2;
        frac2.m_num = 7;
        frac2.m_denom = 11;

        Fraction result = frac1 / frac2;

        int expectedNum = 33, expectedDenom = 35;
        int actualNum = result.m_num, actualDenom = result.m_denom;

        if      ( actualNum != expectedNum )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected numerator!" << endl;
        }
        else if ( actualDenom != expectedDenom )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected denominator!" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 / frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedNum << ", " << expectedDenom << endl;
            cout << "\t actual output:   " << actualNum << ", " << actualDenom <<  endl;
        }
    } // test end

    {
        cout << "TEST 2... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;
        frac1.m_num = 1;
        frac1.m_denom = 2;

        Fraction frac2;
        frac2.m_num = 1;
        frac2.m_denom = 4;

        Fraction result = frac1 / frac2;

        int expectedNum = 4, expectedDenom = 2;
        int actualNum = result.m_num, actualDenom = result.m_denom;

        if      ( actualNum != expectedNum )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected numerator!" << endl;
        }
        else if ( actualDenom != expectedDenom )
        {
            fail = true;
            cout << "!!! FAIL - Unexpected denominator!" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 / frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedNum << ", " << expectedDenom << endl;
            cout << "\t actual output:   " << actualNum << ", " << actualDenom <<  endl;
        }
    } // test end
}

void FractionTester::Test_IsEqual()
{
    cout << endl << string( 80, '-' ) << "* Test_IsEqual" << endl;

    {
        cout << "TEST 1... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;
        frac1.m_num = 3;
        frac1.m_denom = 5;

        Fraction frac2;
        frac2.m_num = 7;
        frac2.m_denom = 11;

        bool expectedResult = false, actualResult = (frac1 == frac2);

        if  ( actualResult != expectedResult )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 == frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedResult << endl;
            cout << "\t actual output:   " << actualResult <<  endl;
        }
    } // test end

    {
        cout << "TEST 2... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;
        frac1.m_num = 1;
        frac1.m_denom = 2;

        Fraction frac2;
        frac2.m_num = 2;
        frac2.m_denom = 4;

        bool expectedResult = true, actualResult = (frac1 == frac2);

        if  ( actualResult != expectedResult )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 == frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedResult << endl;
            cout << "\t actual output:   " << actualResult <<  endl;
        }
    } // test end
}

void FractionTester::Test_IsNotEqual()
{
    cout << endl << string( 80, '-' ) << "* Test_IsNotEqual" << endl;

    {
        cout << "TEST 1... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;
        frac1.m_num = 3;
        frac1.m_denom = 5;

        Fraction frac2;
        frac2.m_num = 7;
        frac2.m_denom = 11;

        bool expectedResult = true, actualResult = (frac1 != frac2);

        if  ( actualResult != expectedResult )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 != frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedResult << endl;
            cout << "\t actual output:   " << actualResult <<  endl;
        }
    } // test end

    {
        cout << "TEST 2... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;
        frac1.m_num = 1;
        frac1.m_denom = 2;

        Fraction frac2;
        frac2.m_num = 2;
        frac2.m_denom = 4;

        bool expectedResult = false, actualResult = (frac1 != frac2);

        if  ( actualResult != expectedResult )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 != frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedResult << endl;
            cout << "\t actual output:   " << actualResult <<  endl;
        }
    } // test end
}

void FractionTester::Test_IsLessThan()
{
    cout << endl << string( 80, '-' ) << "* Test_IsLessThan" << endl;

    {
        cout << "TEST 1... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;         // 2/4
        frac1.m_num = 1;
        frac1.m_denom = 2;

        Fraction frac2;         // 3/4
        frac2.m_num = 3;
        frac2.m_denom = 4;

        bool expectedResult = true, actualResult = (frac1 < frac2);

        if  ( actualResult != expectedResult )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 < frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedResult << endl;
            cout << "\t actual output:   " << actualResult <<  endl;
        }
    } // test end

    {
        cout << "TEST 2... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;
        frac1.m_num = 1;            // 2/4
        frac1.m_denom = 2;

        Fraction frac2;             // 1/4
        frac2.m_num = 1;
        frac2.m_denom = 4;

        bool expectedResult = false, actualResult = (frac1 < frac2);

        if  ( actualResult != expectedResult )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 < frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedResult << endl;
            cout << "\t actual output:   " << actualResult <<  endl;
        }
    } // test end

    {
        cout << "TEST 3... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;
        frac1.m_num = 1;            // 2/4
        frac1.m_denom = 2;

        Fraction frac2;             // 2/4
        frac2.m_num = 2;
        frac2.m_denom = 4;

        bool expectedResult = false, actualResult = (frac1 < frac2);

        if  ( actualResult != expectedResult )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 < frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedResult << endl;
            cout << "\t actual output:   " << actualResult <<  endl;
        }
    } // test end
}

void FractionTester::Test_IsGreaterThan()
{
    cout << endl << string( 80, '-' ) << "* Test_IsGreaterThan" << endl;

    {
        cout << "TEST 1... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;         // 2/4
        frac1.m_num = 1;
        frac1.m_denom = 2;

        Fraction frac2;         // 3/4
        frac2.m_num = 3;
        frac2.m_denom = 4;

        bool expectedResult = false, actualResult = (frac1 > frac2);

        if  ( actualResult != expectedResult )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 > frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedResult << endl;
            cout << "\t actual output:   " << actualResult <<  endl;
        }
    } // test end

    {
        cout << "TEST 2... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;         // 2/4
        frac1.m_num = 1;
        frac1.m_denom = 2;

        Fraction frac2;         // 1/4
        frac2.m_num = 1;
        frac2.m_denom = 4;

        bool expectedResult = true, actualResult = (frac1 > frac2);

        if  ( actualResult != expectedResult )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 > frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedResult << endl;
            cout << "\t actual output:   " << actualResult <<  endl;
        }
    } // test end

    {
        cout << "TEST 3... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;         // 2/4
        frac1.m_num = 1;
        frac1.m_denom = 2;

        Fraction frac2;         // 2/4
        frac2.m_num = 2;
        frac2.m_denom = 4;

        bool expectedResult = false, actualResult = (frac1 > frac2);

        if  ( actualResult != expectedResult )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 > frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedResult << endl;
            cout << "\t actual output:   " << actualResult <<  endl;
        }
    } // test end
}

void FractionTester::Test_IsLessThanOrEqualTo()
{
    cout << endl << string( 80, '-' ) << "* Test_IsLessThanOrEqualTo" << endl;

    {
        cout << "TEST 1... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;         // 2/4
        frac1.m_num = 1;
        frac1.m_denom = 2;

        Fraction frac2;
        frac2.m_num = 3;
        frac2.m_denom = 4;

        bool expectedResult = true, actualResult = (frac1 <= frac2);

        if  ( actualResult != expectedResult )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 <= frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedResult << endl;
            cout << "\t actual output:   " << actualResult <<  endl;
        }
    } // test end

    {
        cout << "TEST 2... ";
        s_testCount++;
        bool fail = false;
        Fraction frac1;     // 2/4
        frac1.m_num = 1;
        frac1.m_denom = 2;

        Fraction frac2;
        frac2.m_num = 1;
        frac2.m_denom = 4;

        bool expectedResult = false, actualResult = (frac1 <= frac2);

        if  ( actualResult != expectedResult )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 <= frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedResult << endl;
            cout << "\t actual output:   " << actualResult <<  endl;
        }
    } // test end

    {
        cout << "TEST 3... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;     // 2/4
        frac1.m_num = 1;
        frac1.m_denom = 2;

        Fraction frac2;
        frac2.m_num = 2;
        frac2.m_denom = 4;

        bool expectedResult = true, actualResult = (frac1 <= frac2);

        if  ( actualResult != expectedResult )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 <= frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedResult << endl;
            cout << "\t actual output:   " << actualResult <<  endl;
        }
    } // test end
}

void FractionTester::Test_IsGreaterThanOrEqualTo()
{
    cout << endl << string( 80, '-' ) << "* Test_IsGreaterThanOrEqualTo" << endl;

    {
        cout << "TEST 1... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;         // 2/4
        frac1.m_num = 1;
        frac1.m_denom = 2;

        Fraction frac2;
        frac2.m_num = 3;
        frac2.m_denom = 4;

        bool expectedResult = false, actualResult = (frac1 >= frac2);

        if  ( actualResult != expectedResult )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 >= frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedResult << endl;
            cout << "\t actual output:   " << actualResult <<  endl;
        }

    } // test end

    {
        cout << "TEST 2... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;     // 2/4
        frac1.m_num = 1;
        frac1.m_denom = 2;

        Fraction frac2;
        frac2.m_num = 1;
        frac2.m_denom = 4;

        bool expectedResult = true, actualResult = (frac1 >= frac2);

        if  ( actualResult != expectedResult )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 >= frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedResult << endl;
            cout << "\t actual output:   " << actualResult <<  endl;
        }

    } // test end

    {
        cout << "TEST 3... ";
        s_testCount++;
        bool fail = false;

        Fraction frac1;     // 2/4
        frac1.m_num = 1;
        frac1.m_denom = 2;

        Fraction frac2;
        frac2.m_num = 2;
        frac2.m_denom = 4;

        bool expectedResult = true, actualResult = (frac1 >= frac2);

        if  ( actualResult != expectedResult )
        {
            fail = true;
            cout << "!!! FAIL" << endl;
        }
        else
        {
            cout << "PASS" << endl;
            s_score++;
        }

        if ( fail )
        {
            cout << "\t input:  frac1 >= frac2" << endl;
            cout << "\t frac1: " << frac1 << endl;
            cout << "\t frac2: " << frac2 << endl;
            cout << "\t expected output: " << expectedResult << endl;
            cout << "\t actual output:   " << actualResult <<  endl;
        }

    } // test end
}
