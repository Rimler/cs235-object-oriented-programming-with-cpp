#include "Fraction.h"

Fraction::Fraction()
{
}

Fraction::Fraction( int num, int denom )
{
}

Fraction::Fraction( const Fraction& other )
{
}

void Fraction::Set( int num, int denom )
{
}

void Fraction::SetNumerator( int num )
{
}

int Fraction::GetNumerator() const
{
    return -1; // TEMPORARY PLACEHOLDER - DELETE ME!
}

void Fraction::SetDenominator( int denom )
{
}

int Fraction::GetDenominator() const
{
    return -1; // TEMPORARY PLACEHOLDER - DELETE ME!
}

float Fraction::GetDecimal() const
{
    return -1; // TEMPORARY PLACEHOLDER - DELETE ME!
}

Fraction Fraction::CommonDenominatorize( const Fraction& other ) const
{
    return Fraction(); // TEMPORARY PLACEHOLDER - DELETE ME!
}

Fraction& Fraction::operator=( const Fraction& rhs )
{
    return *this; // TEMPORARY PLACEHOLDER - DELETE ME!
}

Fraction operator+( const Fraction& left, const Fraction& right )
{
    return Fraction(); // TEMPORARY PLACEHOLDER - DELETE ME!
}

Fraction operator-( const Fraction& left, const Fraction& right )
{
    return Fraction(); // TEMPORARY PLACEHOLDER - DELETE ME!
}

Fraction operator*( const Fraction& left, const Fraction& right )
{
    return Fraction(); // TEMPORARY PLACEHOLDER - DELETE ME!
}

Fraction operator/( const Fraction& left, const Fraction& right )
{
    return Fraction(); // TEMPORARY PLACEHOLDER - DELETE ME!
}

bool operator==( const Fraction& left, const Fraction& right )
{
    return false; // TEMPORARY PLACEHOLDER - DELETE ME!
}

bool operator!=( const Fraction& left, const Fraction& right )
{
    return false; // TEMPORARY PLACEHOLDER - DELETE ME!
}

bool operator<( const Fraction& left, const Fraction& right )
{
    return false; // TEMPORARY PLACEHOLDER - DELETE ME!
}

bool operator>( const Fraction& left, const Fraction& right )
{
    return false; // TEMPORARY PLACEHOLDER - DELETE ME!
}

bool operator<=( const Fraction& left, const Fraction& right )
{
    return false; // TEMPORARY PLACEHOLDER - DELETE ME!
}

bool operator>=( const Fraction& left, const Fraction& right )
{
    return false; // TEMPORARY PLACEHOLDER - DELETE ME!
}

ostream& operator<<( ostream& out, const Fraction& item )
{
    return out; // TEMPORARY PLACEHOLDER - DELETE ME!
}

istream& operator>>( istream& in, Fraction& item )
{
    return in; // TEMPORARY PLACEHOLDER - DELETE ME!
}
