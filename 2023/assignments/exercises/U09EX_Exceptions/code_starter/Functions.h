#ifndef _FUNCTIONS
#define _FUNCTIONS

#include <array>
#include <string>

#include "Exception.h"

float Divide( float num, float denom );

void Display(std::array<std::string, 5> arr, int index);

void PtrDisplay(int* arr, int size, int index);

int SlicesPerPerson(int friends, int pizzaSlices);

#endif
