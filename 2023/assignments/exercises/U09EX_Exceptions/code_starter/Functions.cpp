#include "Functions.h"
#include <stdexcept>
#include <iostream>

float Divide( float num, float denom )
{
    // TODO: Add exception handling
    return num / denom;
}

void Display(std::array<std::string, 5> arr, int index)
{
    // TODO: Add exception handling
    std::cout << "Item at index " << index << " is " << arr[index] << std::endl;
}

void PtrDisplay(int* arr, int size, int index)
{
    // TODO: Add exception handling
    std::cout << "Item at index " << index << " is " << arr[index] << std::endl;
}

int SlicesPerPerson(int friends, int pizzaSlices)
{
    // TODO: Add exception handling
    return float(pizzaSlices) / friends;
}
